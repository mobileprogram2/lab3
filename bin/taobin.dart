import 'dart:io';

void main() {
  List<Map<String, int>> drinks = [
    {'Hot Espresso': 35},
    {'Hot Americano': 40},
    {'Hot Cafe Latte': 45},
    {'Hot Cappuchino': 45},
    {'Hot Mocha': 40},
    {'Iced Espresso': 35},
    {'Iced Americano': 40},
    {'Iced Cafe Latte': 45},
    {'Iced Mattcha Cafe Latte': 45},
    {'Iced Mocha': 40},
  ];
  List<String> sweetest = [
    'No Sugar 0%',
    'Little Sweet 25%',
    'Sweet fit 50%,',
    'So sweet 75%',
    'Sweetest 100%'
  ];
  List<String> order = [];
  int total = 0;
  int drinkMenu = 0;
  int sweetlevel = 0;
  int money = 0;
  int pay = 0;
  int change = 0;

  while (true) {
    for (int i = 0; i < drinks.length; i++) {
      print(
          "${i + 1} : ${drinks[i].keys.first} price ${drinks[i].values.first} bath");
    }

    drinkMenu = chooseMenu(drinkMenu);

    sweetlevel = chooseSweet(sweetest, sweetlevel);

    addOrder(order, drinkMenu, sweetlevel);

    printOrder(order, total, drinks, sweetest);

   break;
  }
  choosePayment(pay, money, total, change);
    
    waitOrder(order);
 
}

void addOrder(List<String> order, int drinkMenu, int sweetlevel) => order.add('${drinkMenu - 1},${sweetlevel - 1}');

void waitOrder(List<String> order) {
   print("Please wait for a minute. Taobin is create your order");
  for(int i= (10*order.length); i >= 0; i--){
    sleep(Duration(seconds: 1));
    print('wait a $i second.');
  }
  print('Order is succesful, Thank you for usr Taobin service');
}

void choosePayment(int pay, int money, int total, int change) {
  print("Select your payment");
  print("1 : จ่ายด้วยเงินสด");
  print("2 : แสกนQR Code");
  print("3 : จ่ายด้วยShopeePay");
  print("4 : จ่ายด้วยคูปองส่วนลด");

    pay = int.parse(stdin.readLineSync()!);
    while (pay != -1) {
      if (pay == 1) {
        print('จ่ายด้วยเงินสด');
        money = int.parse(stdin.readLineSync()!);
        if (money > total) {
          change = money - total;
          print('เงินทอน $change บาท');
          print('payment succesful');
          break;
        } else if (money == total) {
          print('payment succesful');
          break;
        }
      } else if (pay == 2) {
        print('payment succesful');
        print('Scan QR');
        break;
      } else if (pay == 3) {
        print('payment succesful');
        print('ShoppeePay');
        print('payment succesful');
        break;
      } else if (pay == 4) {
        print('จ่ายด้วยคูปองส่วนลด');
        print('payment succesful');
        break;
      }
      
      break;
    }
    
}

int printOrder(List<String> order, int total, List<Map<String, int>> drinks, List<String> sweetest) {
   print('Your order is');
  for (int i = 0; i < order.length; i++) {
    List<String> last = order[i].split(',');
    total = (drinks[int.parse(last[0])].values.first);
  
    print(
        '${drinks[int.parse(last[0])].keys.first} ${sweetest[int.parse(last[1])]} price ${drinks[int.parse(last[0])].values.first} bath');
  }return total;
}

int chooseSweet(List<String> sweetest, int sweetlevel) {
   while (true) {
    for (int i = 0; i < sweetest.length; i++) {
      print("${i + 1} : ${sweetest[i]}");
    }
    print("Select Sweet level");
    sweetlevel = int.parse(stdin.readLineSync()!);
    if (sweetlevel < 1 || sweetlevel > 5) {
      print("Don't Have this level. Please try again");
    } else {
      break;
    }
  }
  return sweetlevel;
}

int chooseMenu(int drinkMenu) {
  print("Select Menu");
  while (true) {
    drinkMenu = int.parse(stdin.readLineSync()!);
    if (drinkMenu < 1 || drinkMenu > 10) {
      print("Don't Have this Product. Please select new product");
    } else {
      break;
    }
  }
  return drinkMenu;
}
